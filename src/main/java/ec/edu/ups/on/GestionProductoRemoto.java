package ec.edu.ups.on;

import java.util.List;

import javax.ejb.Remote;

import ec.edu.ups.models.Movimiento;
import ec.edu.ups.models.Producto;

@Remote
public interface GestionProductoRemoto {
	
	public List<Producto> listarProductos() throws Exception;
	
	public void setIngreso(int pro_id, int cantidad) throws Exception;
	
	public void setEgreso(int pro_id, int cantidad) throws Exception;
	
	public int getDescripcion();
	
	public void ingresarProducto(Producto producto);

	public List<Movimiento> listarMovimientos(int pro_id);
}