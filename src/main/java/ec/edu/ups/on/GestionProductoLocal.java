package ec.edu.ups.on;

import java.util.List;

import javax.ejb.Local;

import ec.edu.ups.models.Movimiento;
import ec.edu.ups.models.Producto;

@Local
public interface GestionProductoLocal {
	public List<Producto> listarProductos() throws Exception;
	
	public void setIngreso(int pro_id, int cantidad) throws Exception;
	
	public void setEgreso(int pro_id, int cantidad) throws Exception;
	
	public int getDescripcion();
	
	public void ingresarProducto(Producto producto);
	
	public List<Movimiento> listarMovimientos(int pro_id);
}