package ec.edu.ups.on;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.inject.Inject;

import ec.edu.ups.datos.MovimientoDAO;
import ec.edu.ups.datos.ProductoDAO;
import ec.edu.ups.models.Movimiento;
import ec.edu.ups.models.Producto;

/**
 * Esta clase funciona como fachada para 
 * realizar las operaciones de un 
 * proceso de cajero.
 */
@Stateless
public class GestionProducto implements GestionProductoRemoto, GestionProductoLocal, Serializable {

	@Inject 
	private ProductoDAO pdao;
	
	@Inject
	private MovimientoDAO mdao;
	
	/**
	 * Crea una nueva instancia de la clase ProcesoCajeroON. 
	 */
	public GestionProducto() {
	}
	
	public List<Producto> listarProductos() {
		return pdao.listarProductos();
	}

	@Override
	public void setIngreso(int pro_id, int cantidad) throws Exception {
		Producto producto = pdao.buscarProducto(pro_id);
		producto.setPro_stock(producto.getPro_stock()+cantidad);
		pdao.aumentarStock(producto);
		Movimiento movimiento = new Movimiento();
		movimiento.setMov_ingreso(cantidad);
		movimiento.setProducto(producto);
		movimiento.setSaldo(producto.getPro_stock());
		movimiento.setMov_egreso(0);
		movimiento.setMov_descripcion("Ingreso #"+getDescripcion());
		mdao.setIngreso(movimiento);
	}

	@Override
	public void setEgreso(int pro_id, int cantidad) throws Exception {
		Producto producto = pdao.buscarProducto(pro_id);
		producto.setPro_stock(producto.getPro_stock()-cantidad);
		pdao.disminuirStock(producto);
		Movimiento movimiento = new Movimiento();
		movimiento.setMov_ingreso(0);
		movimiento.setProducto(producto);
		movimiento.setSaldo(producto.getPro_stock());
		movimiento.setMov_egreso(cantidad);
		movimiento.setMov_descripcion("Egreso #"+getDescripcion());
		mdao.setEgreso(movimiento);
	}
	
	@Override
	public int getDescripcion() {
		return mdao.getDescripcion()+1;
	}

	@Override
	public void ingresarProducto(Producto producto) {
		pdao.ingresarProducto(producto);
	}

	@Override
	public List<Movimiento> listarMovimientos(int mov_id) {
		return mdao.listarMovimientos(mov_id);
	}
	
	
}