package ec.edu.ups.models;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="Movimientos")
public class Movimiento {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int mov_id;
	
	@Column
	private LocalDate mov_fecha;
	
	@Column
	private String mov_descripcion;
	
	@Column
	private int mov_ingreso;
	
	@Column
	private int mov_egreso;
	
	@Column
	private int saldo;
	
	@ManyToOne
	@JoinColumn(name="producto")
	private Producto producto;
	
	public Movimiento() {
		mov_fecha = LocalDate.now();
	}

	public int getMov_id() {
		return mov_id;
	}

	public void setMov_id(int mov_id) {
		this.mov_id = mov_id;
	}

	public LocalDate getMov_fecha() {
		return mov_fecha;
	}

	public void setMov_fecha(LocalDate mov_fecha) {
		this.mov_fecha = mov_fecha;
	}

	public String getMov_descripcion() {
		return mov_descripcion;
	}

	public void setMov_descripcion(String mov_descripcion) {
		this.mov_descripcion = mov_descripcion;
	}

	public int getMov_ingreso() {
		return mov_ingreso;
	}

	public void setMov_ingreso(int mov_ingreso) {
		this.mov_ingreso = mov_ingreso;
	}

	public int getMov_egreso() {
		return mov_egreso;
	}

	public void setMov_egreso(int mov_egreso) {
		this.mov_egreso = mov_egreso;
	}

	public int getSaldo() {
		return saldo;
	}

	public void setSaldo(int saldo) {
		this.saldo = saldo;
	}

	public Producto getProducto() {
		return producto;
	}

	public void setProducto(Producto producto) {
		this.producto = producto;
	}

	@Override
	public String toString() {
		return "Movimiento [mov_id=" + mov_id + ", mov_fecha=" + mov_fecha + ", mov_descripcion=" + mov_descripcion
				+ ", mov_ingreso=" + mov_ingreso + ", mov_egreso=" + mov_egreso + ", saldo=" + saldo + ", producto="
				+ producto + "]";
	}
}
