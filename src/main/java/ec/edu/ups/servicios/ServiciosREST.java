package ec.edu.ups.servicios;

import java.time.LocalDate;
import java.util.List;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ec.edu.ups.models.Movimiento;
import ec.edu.ups.models.Producto;
import ec.edu.ups.on.GestionProductoLocal;

@Path("/cliente")
public class ServiciosREST{

	@Inject
	private GestionProductoLocal pon;
	
	@GET
	@Path("/getProductos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Producto> getCreditos() throws Exception {
		try {
			return pon.listarProductos();
		} catch (Exception e) {
			throw new Exception("Se ah producido un error"+e.getMessage());
		}
	}
	
	@GET
	@Path("/ingresarProducto")
	@Produces(MediaType.TEXT_PLAIN)
	public String ingresarProducto(@QueryParam("nombre") String nombre,
									@QueryParam("stock") int stock,
									@QueryParam("precio") double precio) throws Exception {
		try {
			Producto producto = new Producto();
			producto.setPro_nombre(nombre);
			producto.setPro_precio(precio);
			producto.setPro_stock(stock);
			pon.ingresarProducto(producto);
			return "exito";
		} catch (Exception e) {
			throw new Exception("Se ah producido un error"+e.getMessage());
		}
	}
	
	@GET
	@Path("/setIngreso")
	@Produces(MediaType.TEXT_PLAIN)
	public String setIngreso(@QueryParam("pro_id") int pro_id,
			@QueryParam("pro_cantidad") int pro_cantidad) throws Exception {
		try {
			pon.setIngreso(pro_id, pro_cantidad);
			return "exito";
		} catch (Exception e) {
			throw new Exception("Se ah producido un error"+e.getMessage());
		}
	}
	
	@GET
	@Path("/setEgreso")
	@Produces(MediaType.TEXT_PLAIN)
	public String setEgreso(@QueryParam("pro_id") int pro_id,
			@QueryParam("pro_cantidad") int pro_cantidad) throws Exception {
		try {
			pon.setEgreso(pro_id, pro_cantidad);
			return "exito";
		} catch (Exception e) {
			throw new Exception("Se ah producido un error"+e.getMessage());
		}
	}
	
	@GET
	@Path("/getProductos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Producto> getProductos() throws Exception {
		try {
			return pon.listarProductos();
		} catch (Exception e) {
			throw new Exception("Se ah producido un error"+e.getMessage());
		}
	}
	
	@GET
	@Path("/getMovimientos")
	@Produces(MediaType.APPLICATION_JSON)
	public List<Movimiento> getMovimientos(@QueryParam("pro_id") int pro_id) throws Exception {
		try {
			List<Movimiento> movimientos = pon.listarMovimientos(pro_id);
			return pon.listarMovimientos(pro_id);
		} catch (Exception e) {
			throw new Exception("Se ah producido un error"+e.getMessage());
		}
	}
}