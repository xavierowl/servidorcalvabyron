package ec.edu.ups.datos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import ec.edu.ups.models.Movimiento;
import ec.edu.ups.models.Producto;

/**
 * Esta clase permite realizar operaciones 
 * de mantenimiento de un Usuario.
 **/
@Stateless
public class MovimientoDAO {
	@PersistenceContext
	private EntityManager manager;
	
	public MovimientoDAO() {
	}
	
	public List<Movimiento> listarMovimientos(int pro_id){
		String jpql = "select m from Movimiento m where m.producto = "+pro_id;
		List<Movimiento> movimientos = manager.createQuery(jpql, Movimiento.class).getResultList();
		return movimientos;		
	}
	
	public void setEgreso(Movimiento movimiento) {
		manager.persist(movimiento);
	}
	
	public void setIngreso(Movimiento movimiento) {
		manager.persist(movimiento);
	}
	
	public int getDescripcion() {
		String jpql = "SELECT COUNT(m.mov_id) FROM Movimiento m";
		return manager.createQuery(jpql, Long.class).getSingleResult().intValue();
	}
}