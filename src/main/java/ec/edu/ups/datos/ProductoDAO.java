package ec.edu.ups.datos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import ec.edu.ups.models.Producto;

/**
 * Esta clase permite realizar las operaciones 
 * de mantenimiento de una Cuenta.
 **/
@Stateless
public class ProductoDAO {
	@PersistenceContext
	private EntityManager manager;
	
	public ProductoDAO() {
	}
	
	public void ingresarProducto(Producto producto) {
		manager.persist(producto);
	}
	
	public List<Producto> listarProductos() {
		String jpql = "select p from Producto p";
		List<Producto> productos = manager.createQuery(jpql, Producto.class).getResultList();
		return productos;
	}
	
	public void disminuirStock(Producto producto) {		
		manager.persist(producto);
	}

	public void aumentarStock(Producto producto) {
		manager.persist(producto);
		//String jpql = "UPDATE Producto p SET p.pro_stock = p.pro_stock +"+cantidad+"where p.pro_id = "+pro_id;
		//manager.createQuery(jpql).executeUpdate();
	}
	
	public Producto buscarProducto(int pro_id) {
		return manager.find(Producto.class, pro_id);
	}
}
